let section = document.querySelector("#mensagens")
let btr_enviar = document.querySelector(".btr_enviar");

btr_enviar.addEventListener("click",()=> {
    let input = document.querySelector("#chat");
    let box = document.createElement("div")
    let msg = document.createElement("p");
    let btr_edit = document.createElement("button");
    let btr_excluir = document.createElement("button");
    
    msg.innerText = input.value;
    btr_edit.innerText = "Editar";
    btr_excluir.innerText = "Excluir";
    
    msg.classList.add("p");
    btr_edit.classList.add("edit");
    btr_excluir.classList.add("excluir");
    box.classList.add("box");

    box.append(msg);
    box.append(btr_edit);
    box.append(btr_excluir);
    section.append(box);

    btr_excluir.onclick = () => {remove(btr_excluir)};
    btr_edit.onclick = () => {btr_edit.parentElement.firstElementChild.innerText = input.value};
    
});
function remove(btr){
    btr.parentElement.remove();
}